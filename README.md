# geoclue-listener

This is a Gnome JavaScript class to make it easier for Gnome shell extensions (or anything else written in GJS) to communicate with Geoclue for geolocation service integrated into the Gnome desktop. Installation is just copying the file into your project.

# API Reference
To use this in a Gnome JavaScript project, import the class from `Me.imports.geocluelistener`.
```javascript
const GeoclueListener = Me.imports.geocluelistener;
```

The steps to use it are usually as follows:
* Instantiate the `Listener` class. The constructor takes a distance threshold in meters. The listener will only emit a new location if it's this many meters away from the previously emitted location.
* Test the `isGeoclueSupported` property to confirm Geoclue is installed. Note that no function calls to Geoclue listener will crash if Geoclue is not supported. Rather, the listener will simply never register a location change. Checking `isGeoclueSupported` lets you know if the reason for this is because it's still waiting for a new location or if it will never find one due to Geoclue not being installed.
* Write a function that accepts latitude and longitude as parameters and assign it to `onLocationChanged` to be notified of location changes.
* Invoke `startListening()` to make it start listening. At this point it should periodically invoke the callback you assigned to `isGeoclueSupported` every time Geoclue discovers a new location.
* Optonally write a function that excepts an exception object and assign it to `onConnectionError()` to be notified of any problems connection to Geoclue after startListening() is invoked. Note that this callback will only be invoked if Geoclue is installed but it can't connect. Use `isGeoclueSupported` to confirm it's not installed.
* Invoke `stopListening()` to make it stop listening.
* Use `isListening` to see if Geoclue listening is currently active.
* Although the `isGeoclueSupported` callback passes the new latitude and longitude, there are also `latitude` and `longitude` properties you can call to get the last location it found. These are initialized to `null` until `startListening()` triggers finding a new location. Calling `stopListening()` will prevent it from finding an updated location, but the last known location will remain stored in `latitude` and `longitude`. IE - They won't revert back to `null`.

For example:
```javascript
const distanceThresholdInMeters = 100;

function _init()
{
    this.geoclueListener = new GeoclueListener.Listener(distanceThresholdInMeters);
    if (this.geoclueListener)
    {
        this.geoclueListener.onLocationChanged = (latitude, longitude) =>
        {
            this._currentLatitude = latitude;
            this._currentLongitude = longitude;
            this.updateUIToReflectNewLocation();
        }
        this.geoclueListener.onConnectionError = (latitude, longitude) =>
        {
            Main.notify("MyApp", _("Geoclue is down."));
        }
        this.geoclueListener.startListening();
    }
    else
        this.fallbackInitializationForNoGeolocationService();
}

function toggleListenerButtonClicked()
{
    if (this.geoclueListener.isListening)
        this.geoclueListener.stopListening();
    else
        this.geoclueListener.startListening();
}

function getCurrentLocation()
{
    return `latitude ${this.geoclueListener.latitude}, longitude ${this.geoclueListener.longitude}`;
}

```
