/*
 *  Geoclue Listener for Gnome JavaScript
 *  - Provides simple API to communicate with Geoclue
 *  - Geolocation services integrated with Gnome desktop
 *  - Ideal for Gnome shell extensions
 *
 * Copyright (C) 2020 David Mooter
 *
 * 
 * This file is part of geoclue-listener.
 *
 * geoclue-listener is free software: you can redistribute it and/or
 * modify it under the terms of the Do What The Fuck You Want To Public
 * License, Version 2, as published by Sam Hocevar.
 *
 * geoclue-listener is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Do What The Fuck You Want To
 * Public License along with geoclue-listener. If not, see
 * <http://www.wtfpl.net/>, or find the COPYING file included in this
 * project's  * original distribution repository located at
 * <https://gitlab.com/theexternvoid/geoclue-listener>.
 * 
 */

const Geoclue = imports.gi.Geoclue;
const GObject = imports.gi.GObject;

let Listener = GObject.registerClass(
{GTypeName: 'GeoclueListener'},
class Listener extends GObject.Object
{
	_init(distanceThreshold)
	{
		super._init();
		this._distanceThreshold = distanceThreshold;
		this._isListening = false;
		this._geoclueStarted = false;
		this._geoclueLocationChangedId = 0;
		this._latitude = null;
		this._longitude = null;
		this.onLocationChanged = null;
	}
	
	destroy()
	{
		this.stopListening();
		super.destroy();
	}
	
	get latitude()
	{
		return this._latitude;
	}
	
	get longitude()
	{
		return this._longitude;
	}
	
	_onLocationChangedPrivate()
	{
		let geoLocation = this._geoclueSimple.location;
		this._latitude = geoLocation.latitude;
		this._longitude = geoLocation.longitude;
		if (this.onLocationChanged != null)
			this.onLocationChanged(this._latitude, this._longitude);
	}
	
	_onGeoclueConstruction(sourceObject, result)
	{
		if (this._isListening)
		{
			try
			{
				this._geoclueSimple = Geoclue.Simple.new_finish(result);
				this._geoclueStarted = true;
				this._geoclueSimple.get_client().distance_threshold = this._distanceThreshold;
				this._onLocationChangedPrivate();
				if (this._geoclueLocationChangedId == 0)
					this._geoclueLocationChangedId = this._geoclueSimple.connect('notify::location', () => this._onLocationChangedPrivate());
			}
			catch (e)
			{
				logError(e, `Failed to connect to Geoclue2 service.`);
				this._geoclueStarted = false;
				this._isListening = false;
			}
		}
	}
	
	get isListening()
	{
		return this._isListening;
	}
	
	get isGeoclueSupported()
	{
		return Geoclue != null;
	}
	
	startListening()
	{
		if (this.isGeoclueSupported && !this._isListening)
		{
			this._isListening = true;
			Geoclue.Simple.new('org.gnome.Shell', Geoclue.AccuracyLevel.EXACT, null, (sourceObject, result)  => this._onGeoclueConstruction(sourceObject, result));
		}
	}
	
	stopListening()
	{
		if (this.isGeoclueSupported && this._geoclueStarted)
		{
			if (this._geoclueLocationChangedId != 0)
			{
				this._geoclueSimple.disconnect(this._geoclueLocationChangedId);
				this._geoclueLocationChangedId = 0;
			}
			this._geoclueStarted = false;
		}
		this._isListening = false;
	}
});
